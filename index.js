var set_modulo = require( './lib/set_modulo' )
var set = require( './lib/set' )
var decrypt = require( './lib/dec' )
var encrypt = require( './lib/enc' )

var enc = [
  {
    clear: 'A',
    key: 'B',
    enc: set_modulo( set.latin_upper ).pos 
  }
]

var dec = [
  {
    cipher: 'A',
    key: 'B',
    dec: set_modulo( set.latin_upper ).neg 
  }
]

encrypt( enc, ( err, res ) => {
  console.log( 'enc', res )
} )

decrypt( dec, ( err, res ) => {
  console.log( 'dec', res )
} )
