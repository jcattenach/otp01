module.exports = function enc( array, callback ) {
  function s( object ) {
    var has_clear = object.clear
    var has_enc = object.enc && typeof object.enc == 'function'
    var has_key = object.key
    if ( has_clear && has_enc && has_key ) {
      try {
        object.cipher = object.enc( object.clear, object.key )
      }
      catch ( e ) {
        object.error = e
      }
    }
    else {
      object.error = new Error( 'incomplete' )
    }
    return object
  }
  callback( null, array.map( s ) )
}
