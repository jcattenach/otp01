module.exports = function ( alphabet ) {
  return {
    pos: function ( t, k ) {
      var start = alphabet.indexOf( t )
      var index = alphabet.indexOf( k )
      var ofset = start + index % alphabet.length
      return alphabet[ofset]
    },
    neg: function ( t, k ) {
      var start = alphabet.indexOf( t )
      var index = alphabet.indexOf( k )
      var ofset 
      if ( index <= start ) {
        ofset = start - index
      }
      else {
        ofset = alphabet.length - start - index
      }
      return alphabet[ofset]
    }
  }
}
