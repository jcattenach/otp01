module.exports = function dec( array, callback ) {
  function s( object ) {
    var has_cipher = object.cipher
    var has_dec = object.dec && typeof object.dec == 'function'
    var has_key = object.key
    if ( has_cipher && has_dec && has_key ) {
      try {
        object.clear = object.dec( object.cipher, object.key )
      }
      catch ( e ) {
        object.error = e
      }
    }
    else {
      object.error = new Error( 'incomplete' )
    }
    return object
  }
  callback( null, array.map( s ) )
}
